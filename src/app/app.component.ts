import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-root',
  template: `
  <div *ngIf="!won">
    <div class="top-bar">
      <div class="deck"><img src="/assets/Deck.png" />{{deck.length}}</div>
      <div> {{time}}</div>
    </div>
    <div class="card-container">
      <div *ngFor="let card of current;index as i" class="card">
        <card [arr]="card" [selected]="selected.indexOf(i)!=-1" [show]="toShow.indexOf(i)!=-1" (click)="select(card , i)"></card>
      </div>
    </div>
    <div class="buttom-bar">
      <div class="button" (click)="show()">show set</div>
      <div class="button" (click)="setupGame()">restart</div>
    </div>
  </div>
  <div class="won" *ngIf="won">
    <div class="title">your time is : {{result}}</div>
    <div class="button" (click)="setupGame()" > restart </div>
  </div>
  `,
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  deck;
  current;
  toShow = [];
  shown = 0;
  selected = [];
  startTime;
  endTime;
  won;
  result;
  interval;
  time;

  setupGame() {
    this.time = 0;
    this.toShow = [];
    this.selected = [];
    this.shown = 0;
    let cards = generateAllCards(4, 3);
    let shuffled = shuffle(cards);
    this.current = shuffled.splice(0, 12);
    this.deck = shuffled;
    while (!checkIfSetExists(this.current)) {
      this.addCards(3)
    }
    this.startTime = Date.now();
    this.won = false;
    if (this.interval == undefined) {
      clearInterval(this.interval);
      this.interval = undefined;
    }
    this.interval = setInterval(() => {
      this.time = Math.floor((Date.now() - this.startTime) / 1000) + 20 * this.shown;
    }, 1000)
  }

  ngOnInit() {

    this.setupGame();
  }

  addCards(count) {

    this.current = this.current.concat(this.deck.splice(0, count));
  }

  select(card, index) {
    if (this.selected.indexOf(index) != -1) {
      this.selected.splice(this.selected.indexOf(index), 1)
      return;
    }
    if (this.selected.length == 3) {
      this.selected.splice(0, 1);
    }
    this.selected.push(index);
    if (this.selected.length == 3) {
      let selectedCards = this.selected.map((i) => this.current[i]);
      if (checkIfSet(selectedCards[0], selectedCards[1], selectedCards[2])) {
        this.toShow = [];
        this.selected.sort((a, b) => b - a);
        this.selected.forEach((i) => {
          this.current.splice(i, 1);
        })
        this.selected = [];
        if (!checkIfSetExists(this.current) && this.deck.length <= 0) {
          return this.setWinner();
        }
        while (!checkIfSetExists(this.current) || this.current.length < 12 && this.deck.length > 0) {
          this.addCards(3)
        }
      }
    }
  }

  setWinner() {
    this.won = true;
    this.endTime = Date.now();
    this.result = ((this.endTime - this.startTime) / 1000) + this.shown * 20;
  }

  show() {
    this.toShow = checkIfSetExists(this.current) as any;
    this.shown++;
  }


}

@Component({
  selector: 'card',
  template: `
  <div class="card-pic" [ngClass]="{'selected':selected , 'show':show}">
    <div class="shape" [ngClass]="{ 'not-visable':cardArr[0]==0 }">
      <img [src]="'/assets/cards/'+cardArr[1]+'-'+cardArr[2]+'-'+cardArr[3]+'.svg'"/>
    </div>
    <div class="shape" [ngClass]="{ 'not-visable':cardArr[0]==1 }">
      <img [src]="'/assets/cards/'+cardArr[1]+'-'+cardArr[2]+'-'+cardArr[3]+'.svg'"/>
    </div>
    <div class="shape" [ngClass]="{ 'not-visable':cardArr[0]==0 }">
      <img [src]="'/assets/cards/'+cardArr[1]+'-'+cardArr[2]+'-'+cardArr[3]+'.svg'"/>
    </div>
  </div>`
})
export class Card {
  id;
  cardArr;
  @Input() set arr(val) {
    this.cardArr = val;
    this.id = cardToID(val);
  }
  @Input() selected;
  @Input() show;
}





function generateAllCards(length, optionsCount) {
  if (length == 1) {
    let arr = []
    for (let i = 0; i < optionsCount; i++) {
      arr.push([i]);
    }
    return arr;
  }
  else {
    let arr = generateAllCards(length - 1, optionsCount);
    let toReturn = [];
    arr.forEach((a) => {
      for (let i = 0; i < optionsCount; i++) {
        toReturn.push(a.concat([i]))
      }
    })
    return toReturn;
  }
}

function shuffle(arr) {
  let newArr = [];
  let indexes = [];
  for (let a = 0; a < arr.length; a++) {
    indexes.push(a);
  }
  arr.forEach((x, i) => {
    let randIndex = Math.floor(Math.random() * indexes.length);
    newArr[indexes[randIndex]] = x;
    indexes.splice(randIndex, 1);
  })
  return newArr;
}

function completeSet(a, b, index = 0) {
  if (index == a.length) {
    return []
  }
  if (a[index] == b[index]) {
    return [a[index]].concat(completeSet(a, b, index + 1))
  } else {
    return [3 ^ a[index] ^ b[index]].concat(completeSet(a, b, index + 1))
  }
}

function checkIfSet(a, b, c) {
  let predict = completeSet(a, b);
  for (let i = 0; i < c.length; i++) {
    if (c[i] != predict[i]) {
      return false;
    }
  }
  return true;
}

function cardToID(card) {
  return card.join("-");
}

function checkIfSetExists(cards) {
  let obj = {};
  cards.forEach((c) => { obj[cardToID(c)] = true })
  for (let i = 0; i < cards.length - 2; i++) {
    for (let j = i + 1; j < cards.length - 1; j++) {
      let card = completeSet(cards[i], cards[j]);
      let id = cardToID(card);
      if (obj[id]) {
        return [card, cards[i], cards[j]].map((card) => cards.findIndex((c) => {
          for (let i = 0; i < c.length; i++) {
            if (c[i] != card[i]) {
              return false
            }
          }
          return true;
        }));
      }
    }
  }
  return false;
}


