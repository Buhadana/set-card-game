

let express = require('express');


let app = express();

app.use(express.static('./dist/'));

app.listen(process.env.PORT || 8080, () => {
    console.log("listening on " + (process.env.PORT || 8080));
})


